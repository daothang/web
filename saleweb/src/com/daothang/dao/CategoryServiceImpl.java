package com.daothang.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daothang.domain.Category;
@Service
public class CategoryServiceImpl implements CategoryService{
    @Autowired
    private CategoryDAO categoryDAO;
    
	@Override
	public boolean create(Category object) {
		// TODO Auto-generated method stub
		return categoryDAO.create(object);
	}

	@Override
	public boolean update(Category object) {
		// TODO Auto-generated method stub
		return categoryDAO.update(object);
	}

	@Override
	public boolean delete(Category object) {
		// TODO Auto-generated method stub
		return categoryDAO.delete(object);
	}

	@Override
	public Category findById(int categoryId) {
		// TODO Auto-generated method stub
		return categoryDAO.findById(categoryId);
	}

	@Override
	public List<Category> getAll() {
		// TODO Auto-generated method stub
		return categoryDAO.getAll();
	}

}

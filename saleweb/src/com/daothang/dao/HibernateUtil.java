package com.daothang.dao;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static  SessionFactory sessionFactory = null;
//    private static Configuration cfg= null;
 
    static {
        try {
//            cfg = new Configuration();
//            cfg.configure("/com/daothang//hibernate.cfg.xml");
//            sessionFactory = cfg.buildSessionFactory();
        	sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (HibernateException ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
 
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
}

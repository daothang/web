package com.daothang.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.daothang.domain.Product;

public class ProductDAOImpl implements ProductDAO{

	@Override
	public boolean create(Product object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(object);
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return false;
	}

	@Override
	public boolean update(Product object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(object);
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return false;
	}

	@Override
	public boolean delete(Product object) {
		 Session session = HibernateUtil.getSessionFactory().openSession();
	        Transaction transaction = null;
	        try {
	            transaction = session.beginTransaction();
	            session.delete(object);
	            transaction.commit();
	            return true;
	        } catch (Exception ex) {
	            if (transaction != null) {
	                transaction.rollback();
	            }
	            ex.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	        return false;
	}

	@Override
	public Product findById(long productId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM product WHERE productId = :productId");
            query.setLong("productId", productId);
            Product obj = (Product) query.uniqueResult();
            transaction.commit();
            return obj;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return null;
	}

	@Override
	public List<Product> getListByCategory(long categoryId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM product WHERE categoryId = :categoryId");
            query.setLong("categoryId", categoryId);
            List<Product> list = query.list();
            transaction.commit();
            return list;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return null;
	}

	@Override
	public List<Product> getListByCategoryAndLimit(long categoryId, int limit) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM product WHERE categoryId = :categoryId");
            query.setLong("categoryId", categoryId);
            query.setMaxResults(limit);
            List<Product> list = query.list();
            transaction.commit();
            return list;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return null;
	}

	@Override
	public List<Product> getListFeatured(int limit) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM product ORDER BY productView DESC");
            query.setMaxResults(limit);
            List<Product> list = query.list();
            transaction.commit();
            return list;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            
            session.close();
        }
        return null;
	}

	@Override
	public List<Product> getListSale(int limit) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM product ORDER BY productSale DESC");
            query.setMaxResults(limit);
            List<Product> list = query.list();
            transaction.commit();
            return list;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {

            session.close();
        }
        return null;
	}

	@Override
	public List<Product> getListNav(int start, int limit) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM product");
            query.setMaxResults(limit);
            List<Product> list = query.list();
            transaction.commit();
            return list;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return null;

	}


	@Override
	public List<Product> searchStr(String name) {
		Transaction transaction = null;
		try(Session session=HibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			String hql="FROM product E WHERE productName LIKE :name";
			Query query = session.createQuery(hql);
			query.setParameter("name","%"+name+"%");
			List results = query.list();
			return results;
		
	}catch (Exception ex) {
		if (transaction != null) {
            transaction.rollback();
        }
            ex.printStackTrace();
	}
        return null;
	}

	@Override
	public int count() {
		Transaction transaction = null;
		try(Session session=HibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			String hql="select count(e.productID) from product e";
			Query query = session.createQuery(hql);
			return ((Number) query.uniqueResult()).intValue();
		
	}catch (Exception ex) {
		if (transaction != null) {
//            transaction.rollback();
        }
            ex.printStackTrace();
	}
        return 0;
	
	}

	@Override
	public List<Product> getListLT(int index, int size) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM product");
            query.setFirstResult((index-1)*size);
            query.setMaxResults(size);
            List<Product> list = query.list();
            transaction.commit();
            return list;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return null;
	}

	@Override
	public int pagecount(int total, int size) {
		// TODO Auto-generated method stub
		return (int)Math.ceil(total/(float)size);
	}



	

}

package com.daothang.dao;

import java.util.List;

import com.daothang.domain.Product;

public interface ProductService {
	// create
    public boolean create(Product object);
 
    // update
    public boolean update(Product object);
 
    // delete
    public boolean delete(Product object);
 
    // find by id
    public Product findById(long productId);
 
    // load list product by category
    public List<Product> getListByCategory(long categoryId);
 
    // load list product by featured
    public List<Product> getListFeatured(int limit);
 
    // load list product by sale
    public List<Product> getListSale(int limit);
 
    // load list product by nav
    public List<Product> getListNav(int start, int limit);
 
    //search
    public List<Product> SearchSp(String name);
    
	//count product
	
	
	public int count();
	
	//get list with limited ->to pages
	
	public List<Product> getListLT(int index,int size);
	
	// count pages
	
	public int pagecount(int total, int size);
}

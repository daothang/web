package com.daothang.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.daothang.domain.Product;

public class ProductServiceImpl implements ProductService{
	@Autowired
	private ProductDAO productDAO;
	@Override
	public boolean create(Product object) {
		// TODO Auto-generated method stub
		return productDAO.create(object);
	}

	@Override
	public boolean update(Product object) {
		// TODO Auto-generated method stub
		return productDAO.update(object);
	}

	@Override
	public boolean delete(Product object) {
		// TODO Auto-generated method stub
		return productDAO.delete(object);
	}

	@Override
	public Product findById(long productId) {
		// TODO Auto-generated method stub
		return productDAO.findById(productId);
	}

	@Override
	public List<Product> getListByCategory(long categoryId) {
		// TODO Auto-generated method stub
		return productDAO.getListByCategory(categoryId);
	}

	@Override
	public List<Product> getListFeatured(int limit) {
		// TODO Auto-generated method stub
		return productDAO.getListFeatured(limit);
	}

	@Override
	public List<Product> getListSale(int limit) {
		// TODO Auto-generated method stub
		return productDAO.getListSale(limit);
	}

	@Override
	public List<Product> getListNav(int start, int limit) {
		// TODO Auto-generated method stub
		return productDAO.getListNav(start, limit);
	}

	@Override
	public List<Product> SearchSp(String name) {
		// TODO Auto-generated method stub
		return productDAO.searchStr(name);
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return productDAO.count();
	}

	@Override
	public List<Product> getListLT(int index, int size) {
		// TODO Auto-generated method stub
		return productDAO.getListLT(index, size);
	}

	@Override
	public int pagecount(int total, int size) {
		// TODO Auto-generated method stub
		return productDAO.pagecount(total, size);
	}


	

}

package com.daothang.controller;

import org.apache.tiles.autotag.core.runtime.annotation.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.daothang.dao.CategoryService;
import com.daothang.dao.ProductService;

@Controller
public class HomeController {
    @Autowired
    private CategoryService categoryService;
    
    
    @Autowired
    private ProductService productService;
	@RequestMapping(value="index.html")
	public String index(ModelMap mm) {
		mm.put("listCategory", categoryService.getAll());
	    mm.put("listProductFeatured", productService.getListFeatured(4));
	    mm.put("listProductSale", productService.getListSale(4));
		return "index";
	}
	@RequestMapping(value="category/{categoryUrl}/{categoryId}.html")
	public String viewCategory(ModelMap mm,@PathVariable("categoryUrl") String categoryUrl,@PathVariable("categoryId") long categoryId) {
		mm.put("listCategory", categoryService.getAll());
	    mm.put("listProduct", productService.getListByCategory(categoryId));
	    return "pages.shop";
	}

	@RequestMapping(value = "product/{abc}/{xyz}.html/{productID}")
public String viewProduct(ModelMap mm, @PathVariable("productID") long productId) {
		mm.put("listCategory", categoryService.getAll());
      mm.put("product", productService.findById(productId));
      mm.put("listsale",productService.getListSale(3));
      return "pages.single";	
}
	
	@RequestMapping(value="product/search.html",method=RequestMethod.POST)
	public String search(ModelMap mm,@RequestParam("name") String name) {
		mm.put("listObj", productService.SearchSp(name));
		return "search.product";
	}
}

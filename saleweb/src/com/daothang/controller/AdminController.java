package com.daothang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.daothang.dao.ProductService;
import com.daothang.domain.Product;

@Controller
public class AdminController {
	@Autowired
	private ProductService productservice;
	@RequestMapping("admin/login.html")
	public String login() {
		return "login";
	}
	@RequestMapping("/admin/index.html")
	public String index() {
		return "index.dash";
	}
//	@RequestMapping("/admin/product.html")
//	public String product(Model model) {
//		int size = 6;
//		int p=1;
//		int pages=productservice.pagecount(productservice.count(), size);
//		model.addAttribute("list", productservice.getListLT(p, size));
//		model.addAttribute("pages", pages);
//		return "admin.product";
//	}
	@RequestMapping(value= {"/admin/product.html","/admin/product.html/{p}"})
	public String product(Model model, @PathVariable(value ="p", required=false) Integer p) {
		if (p == null) {
			p = 1;
		}

		int size = 6;
		int pages=productservice.pagecount(productservice.count(), size);
		model.addAttribute("list", productservice.getListLT(p, size));
		model.addAttribute("pages", pages);
		return "admin.product";
	}
}

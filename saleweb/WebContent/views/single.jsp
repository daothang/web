<%-- 
    Document   : single
    Created on : 24-Nov-2018, 11:27:07 AM
    Author     : TVD
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>single</title>
    </head>
    <body>


            <div class="container">
                <div class="single">
                    <div class="col-md-9 top-in-single">
                        <div class="col-md-5 single-top">
                            <img class="img-responsive fashion" src="${pageContext.request.contextPath}/images/${product.productImage}" alt="">
                    </div> 
                    <div class="col-md-7 single-top-in">
                        <div class="single-para">
                            <h4>${product.productName}</h4>
                            <c:set var="check" value="${product.productStatus}"></c:set>
                            <c:if test="${check==true}">
                            <h5>Còn Hàng</h5></c:if>
                            <c:if test="${check==false}">	
                            <h5>Hết Hàng</h5></c:if>
                            <div class="available">
                                <h6>Available Options :</h6>
                                <ul>
                                    <li>Color:
                                        <select>
                                            <option>Silver</option>
                                            <option>Black</option>
                                            <option>Dark Black</option>
                                            <option>Red</option>
                                        </select></li>
                                    <li>Size:<select>
                                            <option>Large</option>
                                            <option>Medium</option>
                                            <option>small</option>
                                            <option>Large</option>
                                            <option>small</option>
                                        </select></li>
                                    <li>Quality:<select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select></li>
                                </ul>
                            </div>
                            <div class="para-grid">
                                <span  class="add-to">$${product.productPrice}</span>
                                <a href="#" class="hvr-shutter-in-vertical cart-to">Add to Cart</a> 
                                <div class="clearfix"></div>
                            </div>
                            <p>${product.productDescription}</p>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                    <%-- <div class="content-top-in">
                        <div class="col-md-4 top-single">
                            <div class="col-md">
                                <img  src="${pageContext.request.contextPath}/resources/pages/images/pic8.jpg" alt="" /> 
                                <div class="top-content">
                                    <h5>Mascot Kitty - White</h5>
                                    <div class="white">
                                        <a href="${pageContext.request.contextPath}/cart/add/${item.productID}.html" class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD TO CART</a>
                                        <p class="dollar"><span class="in-dollar">$</span><span>2</span><span>0</span></p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-4 top-single">
                            <div class="col-md">
                                <img  src="${pageContext.request.contextPath}/resources/pages/images/pic9.jpg" alt="" /> 
                                <div class="top-content">
                                    <h5>Mascot Kitty - White</h5>
                                    <div class="white">
                                        <a href="#" class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD TO CART</a>
                                        <p class="dollar"><span class="in-dollar">$</span><span>2</span><span>0</span></p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-4 top-single-in">
                            <div class="col-md">
                                <img  src="${pageContext.request.contextPath}/resources/pages/images/pic10.jpg" alt="" /> 
                                <div class="top-content">
                                    <h5>Mascot Kitty - White</h5>
                                    <div class="white">
                                        <a href="#" class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD TO CART</a>
                                        <p class="dollar"><span class="in-dollar">$</span><span>2</span><span>0</span></p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
 
                        <div class="clearfix"></div>
                    </div> --%>
                </div>
                <div class="col-md-3">
                    <div class="single-bottom">
                        <h4>Categories</h4>
                        <ul>
                            <li><a href="#"><i> </i>Fusce feugiat</a></li>
                            <li><a href="#"><i> </i>Mascot Kitty</a></li>
                            <li><a href="#"><i> </i>Fusce feugiat</a></li>
                            <li><a href="#"><i> </i>Mascot Kitty</a></li>
                            <li><a href="#"><i> </i>Fusce feugiat</a></li>
                        </ul>
                    </div>
                    <div class="single-bottom">
                        <h4>Product SALE</h4>
                        <c:forEach items="${listsale}" var="list">
                        <div class="product">
                            <img class="img-responsive fashion" src="${pageContext.request.contextPath}/resources/pages/images/st1.jpg" alt="">
                            <div class="grid-product">
                                <a href="#" class="elit">${list.productName}</a>
                                <span class="price price-in"><small>${list.productPrice}</small> ${list.productSale}</span>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        
                        </c:forEach>
                        
                        
                       

                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
</body>
</html>
